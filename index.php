<?php
require_once "controladores/plantilla.controlador.php";
require_once "controladores/usuarios.controlador.php";
require_once "controladores/categorias.controlador.php";
require_once "controladores/productos.controlador.php";
require_once "controladores/clientes.controlador.php";
require_once "controladores/ventas.controlador.php";
require_once "controladores/areas.controlador.php";
require_once "controladores/subareas.controlador.php";
require_once "controladores/unidades.controlador.php";

require_once "modelos/usuarios.modelo.php";
require_once "modelos/categorias.modelo.php";
require_once "modelos/productos.modelo.php";
require_once "modelos/areas.modelo.php";
require_once "modelos/unidades.modelo.php";

$plantilla= new ControladorPlantilla();
$plantilla->ctrPlantilla();