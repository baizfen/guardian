-- --------------------------------------------------------
-- Host:                         127.0.0.1
-- Versión del servidor:         10.1.36-MariaDB - mariadb.org binary distribution
-- SO del servidor:              Win32
-- HeidiSQL Versión:             10.2.0.5599
-- --------------------------------------------------------

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET NAMES utf8 */;
/*!50503 SET NAMES utf8mb4 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;


-- Volcando estructura de base de datos para guardia
CREATE DATABASE IF NOT EXISTS `guardia` /*!40100 DEFAULT CHARACTER SET utf8 COLLATE utf8_spanish2_ci */;
USE `guardia`;

-- Volcando estructura para tabla guardia.areas
CREATE TABLE IF NOT EXISTS `areas` (
  `id_area` int(11) NOT NULL AUTO_INCREMENT,
  `area` text COLLATE utf8_spanish2_ci,
  PRIMARY KEY (`id_area`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_spanish2_ci;

-- Volcando datos para la tabla guardia.areas: ~0 rows (aproximadamente)
/*!40000 ALTER TABLE `areas` DISABLE KEYS */;
/*!40000 ALTER TABLE `areas` ENABLE KEYS */;

-- Volcando estructura para tabla guardia.camas
CREATE TABLE IF NOT EXISTS `camas` (
  `id_cama` int(11) NOT NULL AUTO_INCREMENT,
  `id_unidad` int(11) DEFAULT NULL,
  `id_nom_cama` int(11) DEFAULT NULL,
  `cama` varchar(10) COLLATE utf8_spanish2_ci DEFAULT NULL,
  PRIMARY KEY (`id_cama`),
  UNIQUE KEY `id_unidad` (`id_unidad`),
  UNIQUE KEY `id_nom_cama` (`id_nom_cama`),
  CONSTRAINT `camas_ibfk_1` FOREIGN KEY (`id_nom_cama`) REFERENCES `nomenclatura_camas` (`id_nom_cam`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_spanish2_ci;

-- Volcando datos para la tabla guardia.camas: ~0 rows (aproximadamente)
/*!40000 ALTER TABLE `camas` DISABLE KEYS */;
/*!40000 ALTER TABLE `camas` ENABLE KEYS */;

-- Volcando estructura para tabla guardia.dias_festivos
CREATE TABLE IF NOT EXISTS `dias_festivos` (
  `id_fechafest` int(11) NOT NULL,
  `motivo_fest` text COLLATE utf8_spanish2_ci NOT NULL,
  `fecha` date NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_spanish2_ci;

-- Volcando datos para la tabla guardia.dias_festivos: ~0 rows (aproximadamente)
/*!40000 ALTER TABLE `dias_festivos` DISABLE KEYS */;
/*!40000 ALTER TABLE `dias_festivos` ENABLE KEYS */;

-- Volcando estructura para tabla guardia.estados
CREATE TABLE IF NOT EXISTS `estados` (
  `id_estado` int(11) NOT NULL,
  `nombre_estado` text COLLATE utf8_spanish2_ci NOT NULL,
  `ref_estado` int(11) NOT NULL,
  PRIMARY KEY (`id_estado`),
  UNIQUE KEY `ref_estado` (`ref_estado`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_spanish2_ci;

-- Volcando datos para la tabla guardia.estados: ~0 rows (aproximadamente)
/*!40000 ALTER TABLE `estados` DISABLE KEYS */;
/*!40000 ALTER TABLE `estados` ENABLE KEYS */;

-- Volcando estructura para tabla guardia.nomenclatura_camas
CREATE TABLE IF NOT EXISTS `nomenclatura_camas` (
  `id_nom_cam` int(11) NOT NULL AUTO_INCREMENT,
  `id_subarea` int(11) NOT NULL,
  `nomenclatura` varchar(10) COLLATE utf8_spanish2_ci NOT NULL,
  PRIMARY KEY (`id_nom_cam`),
  UNIQUE KEY `id_subarea` (`id_subarea`),
  CONSTRAINT `nomenclatura_camas_ibfk_1` FOREIGN KEY (`id_subarea`) REFERENCES `subareas` (`id_subarea`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_spanish2_ci;

-- Volcando datos para la tabla guardia.nomenclatura_camas: ~0 rows (aproximadamente)
/*!40000 ALTER TABLE `nomenclatura_camas` DISABLE KEYS */;
/*!40000 ALTER TABLE `nomenclatura_camas` ENABLE KEYS */;

-- Volcando estructura para tabla guardia.permisos
CREATE TABLE IF NOT EXISTS `permisos` (
  `id_permiso` int(11) NOT NULL AUTO_INCREMENT,
  `nom_permiso` text COLLATE utf8_spanish2_ci,
  `permisos` text COLLATE utf8_spanish2_ci,
  PRIMARY KEY (`id_permiso`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_spanish2_ci;

-- Volcando datos para la tabla guardia.permisos: ~0 rows (aproximadamente)
/*!40000 ALTER TABLE `permisos` DISABLE KEYS */;
/*!40000 ALTER TABLE `permisos` ENABLE KEYS */;

-- Volcando estructura para tabla guardia.subareas
CREATE TABLE IF NOT EXISTS `subareas` (
  `id_subarea` int(11) NOT NULL AUTO_INCREMENT,
  `subarea` text COLLATE utf8_spanish2_ci,
  `abrev` text COLLATE utf8_spanish2_ci,
  `id_area` int(11) DEFAULT NULL,
  PRIMARY KEY (`id_subarea`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_spanish2_ci;

-- Volcando datos para la tabla guardia.subareas: ~0 rows (aproximadamente)
/*!40000 ALTER TABLE `subareas` DISABLE KEYS */;
/*!40000 ALTER TABLE `subareas` ENABLE KEYS */;

-- Volcando estructura para tabla guardia.unidades
CREATE TABLE IF NOT EXISTS `unidades` (
  `id_unidad` int(11) NOT NULL AUTO_INCREMENT,
  `nombre` text COLLATE utf8_spanish2_ci,
  `alias` varchar(20) COLLATE utf8_spanish2_ci DEFAULT NULL,
  `clave` varchar(6) COLLATE utf8_spanish2_ci DEFAULT NULL,
  `u_adq` int(11) DEFAULT NULL,
  `municipio` text COLLATE utf8_spanish2_ci,
  `ff` text COLLATE utf8_spanish2_ci,
  `ref_estado` int(11) DEFAULT NULL,
  PRIMARY KEY (`id_unidad`),
  UNIQUE KEY `ref_estado` (`ref_estado`),
  KEY `ref_estado_2` (`ref_estado`),
  CONSTRAINT `unidades_ibfk_1` FOREIGN KEY (`ref_estado`) REFERENCES `estados` (`ref_estado`),
  CONSTRAINT `unidades_ibfk_2` FOREIGN KEY (`id_unidad`) REFERENCES `camas` (`id_unidad`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_spanish2_ci;

-- Volcando datos para la tabla guardia.unidades: ~0 rows (aproximadamente)
/*!40000 ALTER TABLE `unidades` DISABLE KEYS */;
/*!40000 ALTER TABLE `unidades` ENABLE KEYS */;

-- Volcando estructura para tabla guardia.usuarios
CREATE TABLE IF NOT EXISTS `usuarios` (
  `id_usuario` int(11) NOT NULL,
  `id_permiso` int(11) NOT NULL,
  `nombre` text COLLATE utf8_spanish2_ci NOT NULL,
  `matricula` varchar(15) COLLATE utf8_spanish2_ci NOT NULL,
  `password` text COLLATE utf8_spanish2_ci NOT NULL,
  `id_area` int(11) NOT NULL,
  `id_unidad` int(11) NOT NULL,
  `id_subarea` int(11) NOT NULL,
  `foto` text COLLATE utf8_spanish2_ci NOT NULL,
  `estado` int(11) NOT NULL,
  `ultimo_login` datetime NOT NULL,
  `fecha` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00' ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`id_usuario`),
  UNIQUE KEY `id_permiso` (`id_permiso`,`id_area`,`id_unidad`,`id_subarea`),
  KEY `id_unidad` (`id_unidad`),
  KEY `id_area` (`id_area`),
  KEY `id_subarea` (`id_subarea`),
  CONSTRAINT `usuarios_ibfk_1` FOREIGN KEY (`id_permiso`) REFERENCES `permisos` (`id_permiso`),
  CONSTRAINT `usuarios_ibfk_2` FOREIGN KEY (`id_unidad`) REFERENCES `unidades` (`id_unidad`),
  CONSTRAINT `usuarios_ibfk_3` FOREIGN KEY (`id_area`) REFERENCES `areas` (`id_area`),
  CONSTRAINT `usuarios_ibfk_4` FOREIGN KEY (`id_subarea`) REFERENCES `subareas` (`id_subarea`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_spanish2_ci;

-- Volcando datos para la tabla guardia.usuarios: ~0 rows (aproximadamente)
/*!40000 ALTER TABLE `usuarios` DISABLE KEYS */;
/*!40000 ALTER TABLE `usuarios` ENABLE KEYS */;

/*!40101 SET SQL_MODE=IFNULL(@OLD_SQL_MODE, '') */;
/*!40014 SET FOREIGN_KEY_CHECKS=IF(@OLD_FOREIGN_KEY_CHECKS IS NULL, 1, @OLD_FOREIGN_KEY_CHECKS) */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
