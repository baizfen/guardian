<?php

class ControladorUsuarios{

	/*=========================================
	Ingreso de Usuario=========================================*/

	static function ctrIngresoUsuario(){

		if(isset($_POST["ingUsuario"])){

			if(preg_match('/^[a-zA-Z0-9]+$/', $_POST["ingUsuario"]) && preg_match('/^[a-zA-Z0-9*nÑ+]+$/', $_POST["ingPassword"])){

					$encriptar = crypt($_POST["ingPassword"], '$2a$07$asxx54ahjppf45sd87a5a4dDDGsystemdev$');

					//$encriptar = $_POST["ingPassword"];
					$tabla="usuarios";

					$item= "matricula";

					$valor= $_POST["ingUsuario"];

					$respuesta= ModeloUsuarios::MdlMostrarUsuarios($tabla,$item,$valor);

					//var_dump($respuesta["matricula"]);
					if($respuesta["matricula"] == $_POST["ingUsuario"] && $respuesta["password"] == $encriptar)
					{


						if($respuesta["estado"]==1)
						{
							$_SESSION["iniciarSesion"] = "ok";
							$_SESSION["id"] = $respuesta["id"];
							$_SESSION["nombre"] = $respuesta["nombre"];
							$_SESSION["matricula"] = $respuesta["matricula"];
							$_SESSION["foto"] = $respuesta["foto"];
							$_SESSION["permiso"] = $respuesta["permiso"];
							$_SESSION["area"] = $respuesta["id_area"];
							$_SESSION["subarea"] = $respuesta["id_subarea"];
							$_SESSION["unidad"] = $respuesta["id_unidad"];

							/*=================================================================
							=            REGISTRAR FECHA PPARA SABER ULTIMON LOGIN            =
							=================================================================*/
							
							date_default_timezone_set('America/Chihuahua');

							$fecha = date('Y-m-d');
							$hora = date('H:i:s');

							$fechaActual= $fecha.' '.$hora;

							$tabla= "usuarios";

							$item1 = "ultimo_login";

							$valor1 = $fechaActual;

							$item2= "id";

							$valor2 = $respuesta["id"];

							$ultimoLogin = ModeloUsuarios::mdlActualizarUsuario($tabla,$item1,$valor1,$item2,$valor2);
							
							if($ultimoLogin == "ok")
							{
								echo '<script>

								window.location = "inicio";

								</script>';
							}
							
							
						}
						else
						{
							echo "<br><div class='alert alert-danger'>El usuario no esta activado</div>";
						}
					}
					else
					{
						echo "<br><div class='alert alert-danger'>Error al ingresar, vuelve a intentarlo</div>";
					}

			}
		}

	}

	/*===========================================
	=            Registro de usuario            =
	===========================================*/
	
	
		static public function ctrCrearUsuario(){

			if(isset($_POST["nuevoUsuario"]))
			{

				if(preg_match('/^[a-zA-Z0-9ñÑáéíóúÁÉÍÓÚ ]+$/', $_POST["nuevoNombre"]) && preg_match('/^[a-zA-Z0-9]+$/', $_POST["nuevoUsuario"]) && preg_match('/^[a-zA-Z0-9]+$/', $_POST["nuevoPassword"]))
				{
					
					/*======================================
					=            Validar imagen            =
					======================================*/
					$ruta="";
					if(isset($_FILES["nuevaFoto"]["tmp_name"]))
					{
						list($ancho, $alto) = getimagesize($_FILES["nuevaFoto"]["tmp_name"]);
						$nuevAncho=500;
						$nuevoAlto=500;
						/*=========================================================================
						=            Creamo directorio donde vamos a guardar las fotos            =
						=========================================================================*/
						$directorio = "vistas/img/usuarios/".$_POST["nuevoUsuario"];
						mkdir($directorio, 0755);
						/*=============================================
						= De acuerdo al tipo de imagen aplicamos las funciones por defecto de PHP
				           =
						=============================================*/
						
						if($_FILES["nuevaFoto"]["type"] == "image/jpeg"){

							/*============================================================
							=            Guardamos la imagen en el directorio            =
							============================================================*/
							
							$aleatorio= mt_rand(100,999);

							$ruta = "vistas/img/usuarios/".$_POST["nuevoUsuario"]."/".$aleatorio.".jpg";

							$origen = imagecreatefromjpeg($_FILES["nuevaFoto"]["tmp_name"]);
							
							$destino = imagecreatetruecolor($nuevAncho, $nuevoAlto);

							imagecopyresized($destino, $origen, 0, 0, 0, 0, $nuevAncho, $nuevoAlto, $ancho, $alto);

							imagejpeg($destino, $ruta);
						}	
						if($_FILES["nuevaFoto"]["type"] == "image/png"){

							/*============================================================
							=            Guardamos la imagen en el directorio            =
							============================================================*/
							
							$aleatorio= mt_rand(100,999);

							$ruta = "vistas/img/usuarios/".$_POST["nuevoUsuario"]."/".$aleatorio.".png";

							$origen = imagecreatefrompng($_FILES["nuevaFoto"]["tmp_name"]);
							
							$destino = imagecreatetruecolor($nuevAncho, $nuevoAlto);

							imagecopyresized($destino, $origen, 0, 0, 0, 0, $nuevAncho, $nuevoAlto, $ancho, $alto);

							imagepng($destino, $ruta);
						}		
						
						

					}
					
					$tabla="usuarios";

					$encriptar = crypt($_POST["nuevoPassword"], '$2a$07$asxx54ahjppf45sd87a5a4dDDGsystemdev$');

					$datos=array("nombre" => $_POST["nuevoNombre"],
								"usuario" => $_POST["nuevoUsuario"],
								"password" => $encriptar,
								"permiso" => $_POST["nuevoPerfil"],
								"foto" => $ruta);

					$respuesta = ModeloUsuarios::mdlIngresarUsuario($tabla,$datos);
					//var_dump($datos);
					if($respuesta = "ok")
					{

						echo '<script>
						swal({

						type: "success",
						title: "¡El usuario ha sido guardado correctamente!",
						showConfirmButton: true,
						confirmButtonText: "Cerrar"

								}).then(function(result){

									if(result.value){
									window.location="usuarios";
									}


						});
						

						</script>';

					}
					else
					{
						echo '<script>
						swal({

						type: "error",
						title: "¡El usuario no fue grabado!",
						showConfirmButton: true,
						confirmButtonText: "Cerrar"

						}).then(function(result){

								if(result.value){
									window.location="usuarios";
								}


						});
						

						</script>';

					}
					
				}
				else
				{
						echo '<script>
						swal({

						type: "error",
						title: "¡El usuario no puede ir vacío o llevar caracteres especiales!",
						showConfirmButton: true,
						confirmButtonText: "Cerrar"

					}).then(function(result){

								if(result.value){
									window.location="usuarios";
								}


						});
						

					</script>';
				}




			}
		}

/*=======================================
=            Mostrar Usuario            =
=======================================*/


	static public function ctrMostrarUsuario($item, $valor){

		$tabla = "usuarios";

		

		$respuesta= ModeloUsuarios::MdlMostrarUsuarios($tabla,$item,$valor);

		return $respuesta;

	}

/*======================================
=            Editar Usuario            =
======================================*/


	static public function ctrEditarUsuario()
	{

		if(isset($_POST["editarUsuario"])){

			if(preg_match('/^[a-zA-Z0-9ñÑáéíóúÁÉÍÓÚ ]+$/', $_POST["editarNombre"]))
			{

				/*======================================
				=            Validar imagen            =
				======================================*/
				
				
				$ruta= $_POST["fotoActual"];
			if(isset($_FILES["editarFoto"]["tmp_name"]) && !empty($_FILES["editarFoto"]["tmp_name"]))
					{
						list($ancho, $alto) = getimagesize($_FILES["editarFoto"]["tmp_name"]);
						$nuevAncho=500;
						$nuevoAlto=500;
						/*=========================================================================
						=            Creamo directorio donde vamos a guardar las fotos            =
						=========================================================================*/
						$directorio = "vistas/img/usuarios/".$_POST["editarUsuario"];


						/*==========================================================================
						=            Primero preguntamos si existe otra imagen en la BD            =
						==========================================================================*/
						if(!empty($_POST["fotoActual"]))
						{

							unlink($_POST["fotoActual"]);
						}
						{
							mkdir($directorio, 0755);
						}
						
						
						
						/*=============================================
						= De acuerdo al tipo de imagen aplicamos las funciones por defecto de PHP
				           =
						=============================================*/
						
						if($_FILES["editarFoto"]["type"] == "image/jpeg"){

							/*============================================================
							=            Guardamos la imagen en el directorio            =
							============================================================*/
							
							$aleatorio= mt_rand(100,999);

							$ruta = "vistas/img/usuarios/".$_POST["editarUsuario"]."/".$aleatorio.".jpg";

							$origen = imagecreatefromjpeg($_FILES["editarFoto"]["tmp_name"]);
							
							$destino = imagecreatetruecolor($nuevAncho, $nuevoAlto);

							imagecopyresized($destino, $origen, 0, 0, 0, 0, $nuevAncho, $nuevoAlto, $ancho, $alto);

							imagejpeg($destino, $ruta);
						}	
						if($_FILES["editarFoto"]["type"] == "image/png"){

							/*============================================================
							=            Guardamos la imagen en el directorio            =
							============================================================*/
							
							$aleatorio= mt_rand(100,999);

							$ruta = "vistas/img/usuarios/".$_POST["editarUsuario"]."/".$aleatorio.".png";

							$origen = imagecreatefrompng($_FILES["editarFoto"]["tmp_name"]);
							
							$destino = imagecreatetruecolor($nuevAncho, $nuevoAlto);

							imagecopyresized($destino, $origen, 0, 0, 0, 0, $nuevAncho, $nuevoAlto, $ancho, $alto);

							imagepng($destino, $ruta);
						}		
						
						

					}

					$tabla="usuarios";

					if($_POST["editarPassword"]!="")
					{
						if(preg_match('/^[a-zA-Z0-9]+$/', $_POST["editarPassword"]))
						{
							$encriptar = crypt($_POST["editarPassword"], '$2a$07$asxx54ahjppf45sd87a5a4dDDGsystemdev$');
						}
						else
						{
							echo '<script>
						swal({

						type: "success",
						title: "¡La Contraseña no puede llevar caracteres especiales!",
						showConfirmButton: true,
						confirmButtonText: "Cerrar"

								}).then(function(result){

									if(result.value){
									window.location="usuarios";
									}


						});
						

						</script>';
						}
					}
					else
					{
						$encriptar=$_POST["passwordActual"];
					}

					

					$datos=array("nombre" => $_POST["editarNombre"],
								"usuario" => $_POST["editarUsuario"],
								"password" => $encriptar,
								"perfil" => $_POST["editarPerfil"],
								"foto" => $ruta);

					$respuesta = ModeloUsuarios::mdlEditarUsuario($tabla,$datos);

					if($respuesta = "ok")
					{

						echo '<script>
						swal({

						type: "success",
						title: "¡El usuario fue editado correctamente!",
						showConfirmButton: true,
						confirmButtonText: "Cerrar"

								}).then(function(result){

									if(result.value){
									window.location="usuarios";
									}


						});
						

						</script>';

					}
					else
					{
						echo '<script>
						swal({

						type: "error",
						title: "¡El ususuario no fue grabado!",
						showConfirmButton: true,
						confirmButtonText: "Cerrar"

						}).then(function(result){

								if(result.value){
									window.location="usuarios";
								}


						});
						

						</script>';

					}
			}
			else
			{
				echo '<script>
						swal({

						type: "error",
						title: "¡El ususuario no puede ir vacio o con caracteres especiales!",
						showConfirmButton: true,
						confirmButtonText: "Cerrar"

						}).then(function(result){

								if(result.value){
									window.location="usuarios";
								}


						});
						

						</script>';
			}

		}

	}

	/*======================================
	=            Borrar Usuario            =
	======================================*/
	
	static public function ctrBorrarUsuario(){
		if(isset($_GET["idUsuario"]))
		{
			$tabla = "usuarios";

			$datos = $_GET["idUsuario"];

			if($_GET["fotoUsuario"]!="")
			{
				unlink($_GET["fotoUsuario"]);
				rmdir('vistas/img/usuarios/'.$_GET["usuario"]);
			}

			$respuesta= ModeloUsuarios::MdlBorrarUsuario($tabla,$datos);

			if($respuesta = "ok")
			{

						echo '<script>
						swal({

						type: "success",
						title: "¡El usuario ha sido borrado correctamente!",
						showConfirmButton: true,
						confirmButtonText: "Cerrar"

								}).then(function(result){

									if(result.value){
									window.location="usuarios";
									}


						});
						

						</script>';

			}

		}

	}
	
	/*=====  End of Borrar Usuario  ======*/
	

}