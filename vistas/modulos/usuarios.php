
  <div class="content-wrapper">
  
    <section class="content-header">
      <h1>
		Administrar Usuarios

      </h1>
      <ol class="breadcrumb">
        <li><a href="inicio"><i class="fa fa-dashboard"></i> Inicio</a></li>
        <li class="active">Administrar Usuarios</li>
      </ol>
    </section>




    <section class="content">

      
      <div class="box">
        <div class="box-header with-border">
          
          <button class="btn btn-primary" data-toggle="modal" data-target="#modalAgregarUsuario">
            Agregar Usuario
            </button>

        </div>
        <div class="box-body">
          
          <table class="table table-bordered table-striped dt-responsive tablas" width="100%">

            <thead>
              <tr>
                <th style="width: 10px">#</th>
                <th>Nombre</th>
                <th>Usuario</th>
                <th>Foto</th>
                <th>Perfil</th>
                <th>Estado</th>
                <th>Ultimo Login</th>
                <th>Acciones</th>
              </tr>
            </thead>
            <tbody>
              <?php

                $item= null;

                $valor= null;

                $usuarios = ControladorUsuarios::ctrMostrarUsuario($item,$valor);
                
               foreach ($usuarios as $key => $value) {

                echo '

                     <tr>
              
                      <td>'.($key+1).'</td>
                      <td>'.$value["nombre"].'</td>
                      <td>'.$value["matricula"].'</td>';
                      if($value["foto"]!="")
                      {
                       echo '<td><img src="'.$value["foto"].'" class="img-thumbnail" width="40px"></td>';
                      }
                      else
                      {
                       echo '<td><img src="vistas/img/usuarios/default/anonymous.png" class="img-thumbnail" width="40px"></td>'; 
                      }
                      echo '<td>'.$value["permiso"].'</td>';
                      if($value["estado"]!=0)
                      {
                        echo '<td><button class="btn btn-success btn-xs btnActivar" idUsuario="'.$value["id"].'" estadoUsuario="0">Activado</button></td>';
                      }
                      else
                      {
                        echo '<td><button class="btn btn-danger btn-xs btnActivar" idUsuario="'.$value["id"].'" estadoUsuario="1">Desactivado</button></td>';
                      }
                      echo '
                      <td>'.$value["ultimo_login"].'</td>
                      <td>
                      <div class="btn-group">
                        <button class="btn btn-warning btnEditarUsuario" idUsuario="'.$value["id"].'" data-toggle="modal" data-target="#modalEditarUsuario"><i class="fa fa-pencil"></i></button>
                        <button class="btn btn-danger btn-EliminarUsuario" idUsuario="'.$value["id"].'" fotoUsuario="'.$value["foto"].'" usuario="'.$value["matricula"].'"><i class="fa fa-times"></i></button>

                      </div>
                      </td>
                      </tr>


                ';
                
               }

              ?>
           
            
            </tbody>            


          </table>
            
          </div>


        </div>
       
      </div>

    </section>

  </div>

<!--===========================================
=            Modal Agregrar Usuario      =
============================================-->
<div id="modalAgregarUsuario" class="modal fade" role="dialog">
  <div class="modal-dialog">

    <div class="modal-content">
      <form role="form" method="post" enctype="multipart/form-data">
            <div class="modal-header" style="background: #3c8dbc; color: white">

              <button type="button" class="close" data-dismiss="modal">&times;</button>

              <h4 class="modal-title">Agregar usuario</h4>

            </div>

      <!--===========================================
      =           Cuerpo del Modal      =
      ============================================-->


            <div class="modal-body">

      <div class="box-body">

        <!-- Entrada para Nombre -->
        
        <div class="form-group">
          <div class="input-group">
            <span class="input-group-addon"><i class="fa fa-user"></i></span>
            <input type="text" class="form-control input-lg" name="nuevoNombre" placeholder="Ingresar nombre" required>
          </div>
        </div>
      <!-- Entrada para usuario -->

        <div class="form-group">
          <div class="input-group">
            <span class="input-group-addon"><i class="fa fa-key"></i></span>
            <input type="text" class="form-control input-lg" name="nuevoUsuario" placeholder="Ingresar usuario" id="nuevoUsuario" required>
          </div>
        </div>
      </div>
      <!-- Entrada ppara contraseña -->

        <div class="form-group">
          <div class="input-group">
            <span class="input-group-addon"><i class="fa fa-lock"></i></span>
            <input type="password" class="form-control input-lg" name="nuevoPassword" placeholder="Ingresar contraseña" required>
          </div>
        </div>
      <!-- Entarada para seleccionar su perfil -->

        <div class="form-group">
          <div class="input-group">
            <span class="input-group-addon"><i class="fa fa-users"></i></span>
            <select class="form-control input-lg" name="nuevoPerfil" >
              
              <option value="">Seleccionar Perfil</option>
              <option value="Administrador">Administrador</option>
              <option value="Especial">Especial</option>
              <option value="Vendedor">Vendedor</option>
            </select>
          </div>
        </div>
      <!-- Entrada para subir foto -->
      <div class="form-group">
          
            <div class="panel">Subir foto</div>
            <input type="file" class="nuevaFoto" name="nuevaFoto">
            <p class="help-block"> Peso máximo dela foto 2MB</p>
            <img src="vistas/img/usuarios/default/anonymous.png" class="img-thumbnail previsualizar" width="100px">

        </div>

            </div>
     
<!--===================================
=            Pie del modal            =
====================================-->



<!--====  End of Pie del modal  ====-->


      <div class="modal-footer">

        <button type="button" class="btn btn-default pull-left" data-dismiss="modal">Salir</button>

        <button type="submit" class="btn btn.primary">Guardar Usuario</button>

      </div>

<?php
  $crearusuario= new ControladorUsuarios();
  $crearusuario -> ctrCrearUsuario();
?>

   </form>
    </div>

  </div>

</div>


<!--====  End of Agregar Ventana Modal  ====-->



<!--===========================================
=            Modal Editar Usuario      =
============================================-->
<div id="modalEditarUsuario" class="modal fade" role="dialog">
  <div class="modal-dialog">

    <div class="modal-content">
      <form role="form" method="post" enctype="multipart/form-data">
            <div class="modal-header" style="background: #3c8dbc; color: white">

              <button type="button" class="close" data-dismiss="modal">&times;</button>

              <h4 class="modal-title">Editar usuario</h4>

            </div>

      <!--===========================================
      =           Cuerpo del Modal      =
      ============================================-->


            <div class="modal-body">

      <div class="box-body">

        <!-- Entrada para Nombre -->
        
        <div class="form-group">
          <div class="input-group">
            <span class="input-group-addon"><i class="fa fa-user"></i></span>
            <input type="text" class="form-control input-lg" id="editarNombre" name="editarNombre" value="" required>
          </div>
        </div>
      <!-- Entrada para usuario -->

        <div class="form-group">
          <div class="input-group">
            <span class="input-group-addon"><i class="fa fa-key"></i></span>
            <input type="text" class="form-control input-lg" id="editarUsuario" name="editarUsuario" value="" readonly>
          </div>
        </div>
      </div>
      <!-- Entrada ppara contraseña -->

        <div class="form-group">
          <div class="input-group">
            <span class="input-group-addon"><i class="fa fa-lock"></i></span>
            <input type="password" class="form-control input-lg" name="editarPassword" placeholder="Escriba la nueva contraseña">
            <input type="hidden" id="passwordActual" name="passwordActual">
          </div>
        </div>
      <!-- Entarada para seleccionar su perfil -->

        <div class="form-group">
          <div class="input-group">
            <span class="input-group-addon"><i class="fa fa-users"></i></span>
            <select class="form-control input-lg" name="editarPerfil" >
              
              <option value="" id="editarPerfil"></option>
              <option value="Administrador">Administrador</option>
              <option value="Especial">Especial</option>
              <option value="Vendedor">Vendedor</option>
            </select>
          </div>
        </div>

      <!-- Entrada para seleccionar area -->
      <div class="form-group">
          <div class="input-group">
            <span class="input-group-addon"><i class="fa fa-th"></i></span>
            <select class="form-control input-lg" name="editarArea"  required>
              
              <option value="" id="editarArea"></option>
              <?php
                  $item=null;
                  $valor=null;
                  $areas= ControladorAreas::ctrMostrarAreas($item,$valor);

              
              foreach($areas as $key=> $value)
              {
                
                echo '<option value="'.$value["area"].'">'.$value["area"].'</option>';
              }
              ?>
              
            </select>
          </div>
        </div>

         <!-- Entrada para seleccionar area -->
      <div class="form-group">
          <div class="input-group">
            <span class="input-group-addon"><i class="fa fa-th"></i></span>
            <select class="form-control input-lg" name="editarSubarea"  required>
              
             
              
            </select>
          </div>
        </div>

      <!-- Entrada para subir foto -->
      <div class="form-group">
          
            <div class="panel">Subir foto</div>
            <input type="file" class="nuevaFoto" name="editarFoto">
            <p class="help-block"> Peso máximo dela foto 2MB</p>
            <img src="vistas/img/usuarios/default/anonymous.png" class="img-thumbnail previsualizar" width="100px">
            <input type="hidden" name="fotoActual" id="fotoActual">
        </div>

            </div>
     
<!--===================================
=            Pie del modal            =
====================================-->



<!--====  End of Pie del modal  ====-->


      <div class="modal-footer">

        <button type="button" class="btn btn-default pull-left" data-dismiss="modal">Salir</button>

        <button type="submit" class="btn btn.primary">Modificar Usuario</button>

      </div>

<?php

  $editarusuario= new ControladorUsuarios();
  $editarusuario -> ctrEditarUsuario();
?>

   </form>
    </div>

  </div>

</div>


<!--====  End of Agregar Ventana Modal  ====-->

<?php

  $borrarUsuario= new ControladorUsuarios();
  $borrarUsuario -> ctrBorrarUsuario();
?>