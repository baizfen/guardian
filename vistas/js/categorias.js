/*================================================================
=            REVISAR SI LA CATEGORÍA YA ESTA REGISTRADO            =
================================================================*/
$("#nuevaCategoria").change(function(){

	$(".alert").remove();

	var categoria = $(this).val();

	var datos = new FormData();
	datos.append("validarCategoria", categoria);

	 $.ajax({
	    url:"ajax/categorias.ajax.php",
	    method:"POST",
	    data: datos,
	    cache: false,
	    contentType: false,
	    processData: false,
	    dataType: "json",
	    success:function(respuesta){
	    	
	    	if(respuesta){

	    		$("#nuevaCategoria").parent().after('<div class="alert alert-warning">La categoria ya existe en la base de datos</div>');

	    		$("#nuevaCategoria").val("");

	    	}

	    }

	})

})

$("#editarCategoria").change(function(){

	$(".alert").remove();

	var categoria = $(this).val();

	var datos = new FormData();
	datos.append("validarCategoria", categoria);

	 $.ajax({
	    url:"ajax/categorias.ajax.php",
	    method:"POST",
	    data: datos,
	    cache: false,
	    contentType: false,
	    processData: false,
	    dataType: "json",
	    success:function(respuesta){
	    	
	    	if(respuesta){

	    		$("#editarCategoria").parent().after('<div class="alert alert-warning">La categoria ya existe en la base de datos</div>');

	    		$("#editarCategoria").val("");

	    	}

	    }

	})

})

/*=====  End of REVISAR SI LA CATEGORÍA YA ESTA REGISTRADO  ======*/


/*========================================
=            Editar categoria            =
========================================*/
$(".btnEditarCategoria").click(function(){

	var idCategoria=$(this).attr("idCategoria");

	var datos= new FormData();

	datos.append("idCategoria", idCategoria);

	$.ajax({

		url: "ajax/categorias.ajax.php",
		method: "POST",
		data: datos,
		cache: false,
		contentType: false,
		processData: false,
		dataType: "json",
		success: function(respuesta){

			$("#editarCategoria").val(respuesta["categoria"]);
			$("#idCategoria").val(respuesta["id"]);
		}



	})

})



/*=====  End of Editar categoria  ======*/

/*

/*=========================================Eliminar Categoria           =
=============================================*/

$(document).on("click", ".btn-EliminarCategoria", function(){
	var idCategoria	= $(this).attr("idCategoria");

	swal({
		title: '¿Esta seguro de borrar La categoría',
		text: '¡Si no lo está puede cancelar la acción!',
		type: 'warning',
		showCancelButton: true,
		confirmButtonColor: '#3085d6',
		cancelButtonColor: '#d33',
		cancelButtonText: 'Cancelar',
		confirmButtonText: '¡Si, borrar categoría!'
	}).then(function(result){

		if(result.value)
		{
			window.location="index.php?ruta=categorias&idCategoria="+idCategoria;
		}

	})


})