/*==========================================================
=            Cargar tabla dinamica de productos            =
==========================================================*/
// $.ajax({

// 	url: "ajax/datatable-productos.ajax.php",
// 	success:function(respuesta)
// 	{
// 		console.log("respuesta",respuesta);
// 	}

// })


$(document).ready(function() {
    $('.tablaProductos').DataTable( {
        "ajax": "ajax/datatable-productos.ajax.php",
        "deferRender": true,
        "retrieve": true,
        "processing": true,
         "language": {

		"sProcessing":     "Procesando...",
		"sLengthMenu":     "Mostrar _MENU_ registros",
		"sZeroRecords":    "No se encontraron resultados",
		"sEmptyTable":     "Ningún dato disponible en esta tabla",
		"sInfo":           "Mostrando registros del _START_ al _END_ de un total de _TOTAL_",
		"sInfoEmpty":      "Mostrando registros del 0 al 0 de un total de 0",
		"sInfoFiltered":   "(filtrado de un total de _MAX_ registros)",
		"sInfoPostFix":    "",
		"sSearch":         "Buscar:",
		"sUrl":            "",
		"sInfoThousands":  ",",
		"sLoadingRecords": "Cargando...",
		"oPaginate": {
		"sFirst":    "Primero",
		"sLast":     "Último",
		"sNext":     "Siguiente",
		"sPrevious": "Anterior"
		},
		"oAria": {
			"sSortAscending":  ": Activar para ordenar la columna de manera ascendente",
			"sSortDescending": ": Activar para ordenar la columna de manera descendente"
		}

	}
    } );
} );

/*===================================================================
=            Capturando la categoria para asignar codigo            =
===================================================================*/
$("#nuevaCategoria").change(function(){

	var idCategoria = $(this).val();

	var datos= new FormData();

	datos.append("idCategoria",idCategoria);

	$.ajax({
		url: "ajax/productos.ajax.php",
		method: "POST",
		data: datos,
		cache: false,
		contentType: false,
		processData: false,
		dataType: "json",
		success: function(respuesta){
			if(!respuesta)
			{
				var nuevoCodigo=idCategoria+"01";
				$("#nuevoCodigo").val(nuevoCodigo);
			}
			else
			{
				var nuevoCodigo = Number(respuesta["codigo"]) + 1;
				$("#nuevoCodigo").val(nuevoCodigo);
			}
			

		}
	})
})

/*==============================================
=            Agregando Precio Venta            =
==============================================*/

$("#nuevoPrecioCompra, #editarPrecioCompra").change(function(){

	if($(".porcentaje").prop("checked"))
	{
		
		var valorPorcentaje= $(".nuevoPorcentaje").val();
		var porcentaje = Number($("#nuevoPrecioCompra").val()*valorPorcentaje/100)+Number($("#nuevoPrecioCompra").val());
		var editarPorcentaje = Number($("#editarPrecioCompra").val()*valorPorcentaje/100)+Number($("#editarPrecioCompra").val());
		$("#nuevoPrecioVenta").val(porcentaje);
		$("#nuevoPrecioVenta").prop("readonly",true);
		$("#editarPrecioVenta").val(editarPorcentaje);
		$("#editarPrecioVenta").prop("readonly",true);
	}
	else
	{

	}
	

})

/*============================================
=            Cambio de porcentaje            =
============================================*/

$(".nuevoPorcentaje").change(function(){

	if($(".porcentaje").prop("checked"))
	{
		
		var valorPorcentaje= $(".nuevoPorcentaje").val();
		var porcentaje = Number($("#nuevoPrecioCompra").val()*valorPorcentaje/100)+Number($("#nuevoPrecioCompra").val());
		var editarPorcentaje = Number($("#editarPrecioCompra").val()*valorPorcentaje/100)+Number($("#editarPrecioCompra").val());
		$("#editarPrecioVenta").val(editarPorcentaje);
		$("#editarPrecioVenta").prop("readonly",true);
		$("#nuevoPrecioVenta").val(porcentaje);
		$("#nuevoPrecioVenta").prop("readonly",true);

	}
	

})

/*============================================
=            Al quitar check de porcentaje         =
============================================*/
$(".porcentaje").on("ifUnchecked",function(){

	$("#nuevoPrecioVenta").prop("readonly",false);
	$("#editarPrecioVenta").prop("readonly",false);

})
/*============================================
=            Al poner check de porcentaje         =
============================================*/

$(".porcentaje").on("ifChecked",function(){

	
		var valorPorcentaje= $("#nuevoPorcentaje").val();
		var editarValorPorcentaje= $("#editarNuevoPorcentaje").val();
		var porcentaje = Number($("#nuevoPrecioCompra").val()*valorPorcentaje/100)+Number($("#nuevoPrecioCompra").val());
		$("#nuevoPrecioVenta").val(porcentaje);
		$("#nuevoPrecioVenta").prop("readonly",true);
		var editarPorcentaje = Number($("#editarPrecioCompra").val()*editarValorPorcentaje/100)+Number($("#editarPrecioCompra").val());
		$("#editarPrecioVenta").val(editarPorcentaje);
		$("#editarPrecioVenta").prop("readonly",true);
		
		
	
})

/*=============================================
Subiendo la foto del Producto          =
=============================================*/
$(".nuevaImagen").change(function(){

	var imagen= this.files[0];
	//console.log("imagen",imagen);
	/*=============================================
	=  Validamos el fromato de la imagen            =
	=============================================*/
	
	if(imagen["type"] != "image/jpeg" && imagen["type"] != "image/png"){

		$(".nuevaImagen").val("");

		swal({
			title: "Error al subir imagen",
			text: "¡La imagen debe estar en formato JPG o PNG!",
			type: "error",
			confimButtonText: "¡Cerrar!"
		})	;	

	}	
	else if(imagen["size"] > 2000000)
	{
		$(".nuevaImagen").val("");

		swal({
			title: "Error al subir imagen",
			text: "¡La imagen no debe pesar mas de 2MB!",
			type: "error",
			confimButtonText: "¡Cerrar!"
		})	;	

	}
	else
	{
		var datosImagen = new FileReader;
		datosImagen.readAsDataURL(imagen);

		$(datosImagen).on("load",function(event){

			var rutaImagen = event.target.result;

			$(".previsualizar").attr("src",rutaImagen);
		})
	}
	
	
	
})

/*=======================================
=            Editar Producto          Carga hasta despues de imprimir html  =
=======================================*/

$(".tablaProductos").on("click", "button.btnEditarProducto", function(){

	var idProducto = $(this).attr("idProducto");
	
	var datos = new FormData();

	datos.append("idProducto",idProducto);

	$.ajax({
		url:"ajax/productos.ajax.php",
      method: "POST",
      data: datos,
      cache: false,
      contentType: false,
      processData: false,
      dataType:"json",
		success: function(respuesta){
			if(respuesta)
			{
				var datosCategoria = new FormData();
				datosCategoria.append("idCategoria",respuesta["id_categoria"]);

				$.ajax({
					url:"ajax/categorias.ajax.php",
			      method: "POST",
			      data: datosCategoria,
			      cache: false,
			      contentType: false,
			      processData: false,
			      dataType:"json",
			      success: function(respuesta){

			      	$("#editarCategoria").val(respuesta["id"]);
			      	$("#editarCategoria").html(respuesta["categoria"]);

			      }
			  	})

			  	$("#editarCodigo").val(respuesta["codigo"]);
			  	$("#editarDescripcion").val(respuesta["descripcion"]);
			  	$("#editarStock").val(respuesta["stock"]);
			  	$("#editarPrecioCompra").val(respuesta["precio_compra"]);
			  	$("#editarPrecioVenta").val(respuesta["precio_venta"]);
			  	if(respuesta["imagen"]!="")
			  	{
					$("#imagenActual").val(respuesta["imagen"]);
			  		$(".previsualizar").attr("src",respuesta["imagen"]);
			  	}
			  	

			}
			
		}
	})
})
