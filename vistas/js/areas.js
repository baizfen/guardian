/*=======================================
=          Editar        =
=======================================*/

$(document).on("onchange", ".editarArea", function(){

var idUsuario= $(this).attr("idUsuario");
var estadoUsuario= $(this).attr("estadoUsuario");

var datos = new FormData();
datos.append("activarId",idUsuario);
datos.append("activarUsuario",estadoUsuario);

$.ajax({

	url:"ajax/usuarios.ajax.php",
	method: "POST",
	data: datos,
	cache: false,
	contentType: false,
	processData: false,
	success: function(respuesta){
			if(window.matchMedia("(max-width:767px)").matches){

				swal({
					title: "El usuario ha sido actualizado",
					type: "success",
					confimButtonText: "¡Cerrar!",
				}).then(function(result){
					if(result.value)
					{
						window.location = "usuarios";
					}
				})

			}

	}

})