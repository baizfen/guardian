-- --------------------------------------------------------
-- Host:                         127.0.0.1
-- Versión del servidor:         10.1.36-MariaDB - mariadb.org binary distribution
-- SO del servidor:              Win32
-- HeidiSQL Versión:             10.2.0.5599
-- --------------------------------------------------------

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET NAMES utf8 */;
/*!50503 SET NAMES utf8mb4 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;


-- Volcando estructura de base de datos para guardia
CREATE DATABASE IF NOT EXISTS `guardia` /*!40100 DEFAULT CHARACTER SET utf8 COLLATE utf8_spanish2_ci */;
USE `guardia`;

-- Volcando estructura para tabla guardia.areas
CREATE TABLE IF NOT EXISTS `areas` (
  `id_area` int(11) NOT NULL AUTO_INCREMENT,
  `area` text COLLATE utf8_spanish2_ci,
  PRIMARY KEY (`id_area`)
) ENGINE=InnoDB AUTO_INCREMENT=20 DEFAULT CHARSET=utf8 COLLATE=utf8_spanish2_ci;

-- Volcando datos para la tabla guardia.areas: ~19 rows (aproximadamente)
/*!40000 ALTER TABLE `areas` DISABLE KEYS */;
INSERT INTO `areas` (`id_area`, `area`) VALUES
	(1, 'Personal'),
	(2, 'Abastos'),
	(3, 'Servicios Generales'),
	(4, 'Conservacion'),
	(5, 'Nutricion'),
	(6, 'Medicina Preventiva'),
	(7, 'Servicios Integrales'),
	(8, 'Prestaciones Medicas'),
	(9, 'Comunicacion Social'),
	(10, 'Competitividad y Capacitacion'),
	(11, 'Relaciones Laborales'),
	(12, 'SubDelegacion'),
	(13, 'Guarderia'),
	(14, 'Prestaciones Economicas y Sociales'),
	(15, 'Atencion y Orientacion '),
	(16, 'Juridicos'),
	(17, 'Administrativos'),
	(18, 'Supervision de Cobranza'),
	(19, 'Informatica');
/*!40000 ALTER TABLE `areas` ENABLE KEYS */;

-- Volcando estructura para tabla guardia.camas
CREATE TABLE IF NOT EXISTS `camas` (
  `id_cama` int(11) NOT NULL AUTO_INCREMENT,
  `id_unidad` int(11) DEFAULT NULL,
  `id_nom_cama` int(11) DEFAULT NULL,
  `cama` varchar(10) COLLATE utf8_spanish2_ci DEFAULT NULL,
  PRIMARY KEY (`id_cama`),
  UNIQUE KEY `id_unidad` (`id_unidad`),
  UNIQUE KEY `id_nom_cama` (`id_nom_cama`),
  CONSTRAINT `camas_ibfk_1` FOREIGN KEY (`id_nom_cama`) REFERENCES `nomenclatura_camas` (`id_nom_cam`),
  CONSTRAINT `camas_ibfk_2` FOREIGN KEY (`id_unidad`) REFERENCES `unidades` (`id_unidad`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_spanish2_ci;

-- Volcando datos para la tabla guardia.camas: ~0 rows (aproximadamente)
/*!40000 ALTER TABLE `camas` DISABLE KEYS */;
/*!40000 ALTER TABLE `camas` ENABLE KEYS */;

-- Volcando estructura para tabla guardia.casos_riesgo
CREATE TABLE IF NOT EXISTS `casos_riesgo` (
  `id_caso_riesgo` int(11) NOT NULL AUTO_INCREMENT,
  `titulo_caso` text COLLATE utf8_spanish2_ci NOT NULL,
  PRIMARY KEY (`id_caso_riesgo`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_spanish2_ci;

-- Volcando datos para la tabla guardia.casos_riesgo: ~0 rows (aproximadamente)
/*!40000 ALTER TABLE `casos_riesgo` DISABLE KEYS */;
/*!40000 ALTER TABLE `casos_riesgo` ENABLE KEYS */;

-- Volcando estructura para tabla guardia.derechohabiente
CREATE TABLE IF NOT EXISTS `derechohabiente` (
  `id_derechohabiente` int(11) NOT NULL AUTO_INCREMENT,
  `nss` varchar(10) COLLATE utf8_spanish2_ci NOT NULL,
  `agregado` varchar(8) COLLATE utf8_spanish2_ci NOT NULL,
  `id_unidad` int(11) NOT NULL,
  `nombre_pac` text COLLATE utf8_spanish2_ci NOT NULL,
  `consultorio` varchar(3) COLLATE utf8_spanish2_ci NOT NULL,
  `turno` varchar(1) COLLATE utf8_spanish2_ci NOT NULL,
  PRIMARY KEY (`id_derechohabiente`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_spanish2_ci;

-- Volcando datos para la tabla guardia.derechohabiente: ~0 rows (aproximadamente)
/*!40000 ALTER TABLE `derechohabiente` DISABLE KEYS */;
/*!40000 ALTER TABLE `derechohabiente` ENABLE KEYS */;

-- Volcando estructura para tabla guardia.dias_festivos
CREATE TABLE IF NOT EXISTS `dias_festivos` (
  `id_fechafest` int(11) NOT NULL,
  `motivo_fest` text COLLATE utf8_spanish2_ci NOT NULL,
  `fecha` date NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_spanish2_ci;

-- Volcando datos para la tabla guardia.dias_festivos: ~0 rows (aproximadamente)
/*!40000 ALTER TABLE `dias_festivos` DISABLE KEYS */;
/*!40000 ALTER TABLE `dias_festivos` ENABLE KEYS */;

-- Volcando estructura para tabla guardia.estados
CREATE TABLE IF NOT EXISTS `estados` (
  `id_estado` int(11) NOT NULL,
  `nombre_estado` text COLLATE utf8_spanish2_ci NOT NULL,
  `ref_estado` int(11) NOT NULL,
  PRIMARY KEY (`id_estado`),
  UNIQUE KEY `ref_estado` (`ref_estado`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_spanish2_ci;

-- Volcando datos para la tabla guardia.estados: ~3 rows (aproximadamente)
/*!40000 ALTER TABLE `estados` DISABLE KEYS */;
INSERT INTO `estados` (`id_estado`, `nombre_estado`, `ref_estado`) VALUES
	(1, 'Aguascalientes', 1),
	(2, 'Baja California', 2),
	(3, 'Baja California Sur', 3);
/*!40000 ALTER TABLE `estados` ENABLE KEYS */;

-- Volcando estructura para tabla guardia.nomenclatura_camas
CREATE TABLE IF NOT EXISTS `nomenclatura_camas` (
  `id_nom_cam` int(11) NOT NULL AUTO_INCREMENT,
  `id_subarea` int(11) NOT NULL,
  `nomenclatura` varchar(10) COLLATE utf8_spanish2_ci NOT NULL,
  PRIMARY KEY (`id_nom_cam`),
  UNIQUE KEY `id_subarea` (`id_subarea`),
  CONSTRAINT `nomenclatura_camas_ibfk_1` FOREIGN KEY (`id_subarea`) REFERENCES `subareas` (`id_subarea`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_spanish2_ci;

-- Volcando datos para la tabla guardia.nomenclatura_camas: ~0 rows (aproximadamente)
/*!40000 ALTER TABLE `nomenclatura_camas` DISABLE KEYS */;
/*!40000 ALTER TABLE `nomenclatura_camas` ENABLE KEYS */;

-- Volcando estructura para tabla guardia.permisos
CREATE TABLE IF NOT EXISTS `permisos` (
  `id_permiso` int(11) NOT NULL AUTO_INCREMENT,
  `nom_permiso` text COLLATE utf8_spanish2_ci,
  `permisos` text COLLATE utf8_spanish2_ci,
  PRIMARY KEY (`id_permiso`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8 COLLATE=utf8_spanish2_ci;

-- Volcando datos para la tabla guardia.permisos: ~2 rows (aproximadamente)
/*!40000 ALTER TABLE `permisos` DISABLE KEYS */;
INSERT INTO `permisos` (`id_permiso`, `nom_permiso`, `permisos`) VALUES
	(1, 'Superadministrador', '1,2'),
	(2, 'Administrador', '1');
/*!40000 ALTER TABLE `permisos` ENABLE KEYS */;

-- Volcando estructura para tabla guardia.subareas
CREATE TABLE IF NOT EXISTS `subareas` (
  `id_subarea` int(11) NOT NULL AUTO_INCREMENT,
  `subarea` text COLLATE utf8_spanish2_ci,
  `abrev` text COLLATE utf8_spanish2_ci,
  `id_area` int(11) DEFAULT NULL,
  PRIMARY KEY (`id_subarea`)
) ENGINE=InnoDB AUTO_INCREMENT=106 DEFAULT CHARSET=utf8 COLLATE=utf8_spanish2_ci;

-- Volcando datos para la tabla guardia.subareas: ~43 rows (aproximadamente)
/*!40000 ALTER TABLE `subareas` DISABLE KEYS */;
INSERT INTO `subareas` (`id_subarea`, `subarea`, `abrev`, `id_area`) VALUES
	(7, 'Traslados', 'TR', 8),
	(8, 'Especialidades', 'ES', 8),
	(11, 'Servicios Generales', 'SG', 3),
	(12, 'Conservacion', 'CO', 4),
	(16, 'Patologia', 'PA', 8),
	(17, 'Cirugia y Quirofanos', 'CQ', 8),
	(33, 'Farmacia', 'FA', 2),
	(39, 'Nutricion y Dietetica', 'ND', 5),
	(68, 'Jefatura de Pediatria', 'JP', 8),
	(69, 'Coordinacion Medica', 'CM', 8),
	(70, 'Subdireccion Medica', 'SM', 8),
	(71, 'Finanzas', 'FI', 17),
	(72, 'Informatica', 'IN', 19),
	(73, 'Administracion', 'AD', 17),
	(74, 'Abasto', 'CO', 2),
	(75, 'Normativa', 'NO', 8),
	(76, 'Servicios Integrales', 'SI', 7),
	(77, 'Servicios Complementarios', 'SC', 3),
	(78, 'Transportes Y Viaticos', 'TV', 3),
	(79, 'Seguridad Y Resguardo De Bien', 'SR', 3),
	(80, 'Centro De Seguridad Social', 'CS', 14),
	(81, 'Competitividad Y Capacitacion', 'CC', 10),
	(82, 'Prestaciones A Personal', 'PP', 1),
	(83, 'Guarderia Nutricion', 'GN', 13),
	(84, 'Guarderia', 'GU', 13),
	(85, 'Laboratorio', 'LA', 7),
	(86, 'Almacen', 'AL', 2),
	(87, 'Asuntos Sindicales ', 'RL', 11),
	(88, 'Oficina De Capacitacion', 'CA', 1),
	(89, 'Prestaciones Medicas', 'PM', 8),
	(90, 'Economicas Y Sociales', 'ES', 14),
	(92, 'Transportes Y Viaticos', 'TV', 17),
	(93, 'Transportes Y Viaticos', 'TV', 12),
	(94, 'Contratos Honorarios', 'CH', 16),
	(95, 'Soporte Tecnico', 'ST', 19),
	(96, 'CoordinaciÃ³n De InformÃ¡tica', 'CI', 19),
	(97, 'Cobranza', 'CO', 12),
	(100, 'Pensiones', 'PE', 12),
	(101, 'Afiliacion Vigencia', 'AV', 12),
	(102, 'Auditoria Patrones', 'AP', 12),
	(103, 'Salud en el Trabajo', 'ST', 14),
	(104, 'Urgencias', 'UR', 8),
	(105, 'Direccion ', 'DI', 8);
/*!40000 ALTER TABLE `subareas` ENABLE KEYS */;

-- Volcando estructura para tabla guardia.turnos
CREATE TABLE IF NOT EXISTS `turnos` (
  `id_turno` int(11) NOT NULL AUTO_INCREMENT,
  `turno` varchar(20) COLLATE utf8_spanish2_ci NOT NULL,
  PRIMARY KEY (`id_turno`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_spanish2_ci;

-- Volcando datos para la tabla guardia.turnos: ~0 rows (aproximadamente)
/*!40000 ALTER TABLE `turnos` DISABLE KEYS */;
/*!40000 ALTER TABLE `turnos` ENABLE KEYS */;

-- Volcando estructura para tabla guardia.unidades
CREATE TABLE IF NOT EXISTS `unidades` (
  `id_unidad` int(11) NOT NULL AUTO_INCREMENT,
  `nombre` text COLLATE utf8_spanish2_ci,
  `alias` varchar(20) COLLATE utf8_spanish2_ci DEFAULT NULL,
  `clave` varchar(6) COLLATE utf8_spanish2_ci DEFAULT NULL,
  `u_adq` int(11) DEFAULT NULL,
  `municipio` text COLLATE utf8_spanish2_ci,
  `ff` text COLLATE utf8_spanish2_ci,
  `ref_estado` int(11) DEFAULT NULL,
  PRIMARY KEY (`id_unidad`),
  UNIQUE KEY `ref_estado` (`ref_estado`),
  KEY `ref_estado_2` (`ref_estado`),
  CONSTRAINT `unidades_ibfk_1` FOREIGN KEY (`ref_estado`) REFERENCES `estados` (`ref_estado`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8 COLLATE=utf8_spanish2_ci;

-- Volcando datos para la tabla guardia.unidades: ~0 rows (aproximadamente)
/*!40000 ALTER TABLE `unidades` DISABLE KEYS */;
INSERT INTO `unidades` (`id_unidad`, `nombre`, `alias`, `clave`, `u_adq`, `municipio`, `ff`, `ref_estado`) VALUES
	(1, 'Hospital General de Zona + Medicina Familiar #1', 'HGZ +MF #1', '030201', 1, 'La Paz,B.C.S.', '740136', 3);
/*!40000 ALTER TABLE `unidades` ENABLE KEYS */;

-- Volcando estructura para tabla guardia.usuarios
CREATE TABLE IF NOT EXISTS `usuarios` (
  `id_usuario` int(11) NOT NULL,
  `id_permiso` int(11) NOT NULL,
  `nombre` text COLLATE utf8_spanish2_ci NOT NULL,
  `matricula` varchar(15) COLLATE utf8_spanish2_ci NOT NULL,
  `password` text COLLATE utf8_spanish2_ci NOT NULL,
  `id_area` int(11) NOT NULL,
  `id_unidad` int(11) NOT NULL,
  `id_subarea` int(11) NOT NULL,
  `foto` text COLLATE utf8_spanish2_ci NOT NULL,
  `estado` int(11) NOT NULL,
  `ultimo_login` datetime NOT NULL,
  `fecha` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00' ON UPDATE CURRENT_TIMESTAMP,
  `permisos` text COLLATE utf8_spanish2_ci NOT NULL,
  `permisos_temp` text COLLATE utf8_spanish2_ci NOT NULL,
  `fecha_perm_temp` date NOT NULL,
  PRIMARY KEY (`id_usuario`),
  UNIQUE KEY `id_permiso` (`id_permiso`,`id_area`,`id_unidad`,`id_subarea`),
  KEY `id_area` (`id_area`),
  KEY `id_subarea` (`id_subarea`),
  KEY `usuarios_ibfk_2` (`id_unidad`),
  CONSTRAINT `usuarios_ibfk_1` FOREIGN KEY (`id_permiso`) REFERENCES `permisos` (`id_permiso`),
  CONSTRAINT `usuarios_ibfk_2` FOREIGN KEY (`id_unidad`) REFERENCES `unidades` (`id_unidad`),
  CONSTRAINT `usuarios_ibfk_3` FOREIGN KEY (`id_area`) REFERENCES `areas` (`id_area`),
  CONSTRAINT `usuarios_ibfk_4` FOREIGN KEY (`id_subarea`) REFERENCES `subareas` (`id_subarea`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_spanish2_ci;

-- Volcando datos para la tabla guardia.usuarios: ~0 rows (aproximadamente)
/*!40000 ALTER TABLE `usuarios` DISABLE KEYS */;
INSERT INTO `usuarios` (`id_usuario`, `id_permiso`, `nombre`, `matricula`, `password`, `id_area`, `id_unidad`, `id_subarea`, `foto`, `estado`, `ultimo_login`, `fecha`, `permisos`, `permisos_temp`, `fecha_perm_temp`) VALUES
	(1, 1, 'Antonio Baez Angeles', '99031300', '$2a$07$asxx54ahjppf45sd87a5auYoBziSrrssCU/D.Mi5pfcPxIbwcNbMe', 17, 1, 72, 'vistas\\img\\usuarios\\99031300\\113.jpg\r\n', 1, '2020-03-19 18:28:36', '2020-03-19 19:11:23', '1,2', '', '0000-00-00');
/*!40000 ALTER TABLE `usuarios` ENABLE KEYS */;

/*!40101 SET SQL_MODE=IFNULL(@OLD_SQL_MODE, '') */;
/*!40014 SET FOREIGN_KEY_CHECKS=IF(@OLD_FOREIGN_KEY_CHECKS IS NULL, 1, @OLD_FOREIGN_KEY_CHECKS) */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
