<?php

require_once "conexion.php";

class ModeloAreas{

	/*========================================
=            Mostrar Areas            =
========================================*/
	static public function mdlMostrarAreas($tabla,$item,$valor)
	{
		
		if($item != null )
		{
			$stmt = Conexion::conectar()->prepare("SELECT * FROM $tabla WHERE $item = :$item");

			$stmt-> bindParam(":".$item,$valor,PDO::PARAM_STR);

			$stmt->execute();

			return $stmt ->fetch();

			$stmt -> close();

			$stmt = null;	
		}
		else
		{
			$stmt = Conexion::conectar()->prepare("SELECT * FROM $tabla order by area");

			$stmt->execute();

			return $stmt ->fetchAll();

			$stmt -> close();

			$stmt = null;	
		}
	}

}