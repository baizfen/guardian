-- --------------------------------------------------------
-- Host:                         127.0.0.1
-- Versión del servidor:         10.1.36-MariaDB - mariadb.org binary distribution
-- SO del servidor:              Win32
-- HeidiSQL Versión:             10.2.0.5599
-- --------------------------------------------------------

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET NAMES utf8 */;
/*!50503 SET NAMES utf8mb4 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;

-- Volcando datos para la tabla bdimss.areas: 19 rows
/*!40000 ALTER TABLE `areas` DISABLE KEYS */;
INSERT INTO `areas` (`id_area`, `area`) VALUES
	(1, 'Personal'),
	(2, 'Abastos'),
	(3, 'Servicios Generales'),
	(4, 'Conservacion'),
	(5, 'Nutricion'),
	(6, 'Medicina Preventiva'),
	(7, 'Servicios Integrales'),
	(8, 'Prestaciones Medicas'),
	(9, 'Comunicacion Social'),
	(10, 'Competitividad y Capacitacion'),
	(11, 'Relaciones Laborales'),
	(12, 'SubDelegacion'),
	(13, 'Guarderia'),
	(14, 'Prestaciones Economicas y Sociales'),
	(15, 'Atencion y Orientacion '),
	(16, 'Juridicos'),
	(17, 'Administrativos'),
	(18, 'Supervision de Cobranza'),
	(19, 'Informatica');
/*!40000 ALTER TABLE `areas` ENABLE KEYS */;

-- Volcando datos para la tabla bdimss.subareas: ~40 rows (aproximadamente)
/*!40000 ALTER TABLE `subareas` DISABLE KEYS */;
INSERT INTO `subareas` (`id`, `nombre`, `abrev`, `idar`) VALUES
	(7, 'Traslados', 'TR', 8),
	(8, 'Especialidades', 'ES', 8),
	(11, 'Servicios Generales', 'SG', 3),
	(12, 'Conservacion', 'CO', 4),
	(16, 'Patologia', 'PA', 8),
	(17, 'Cirugia y Quirofanos', 'CQ', 8),
	(33, 'Farmacia', 'FA', 2),
	(39, 'Nutricion y Dietetica', 'ND', 5),
	(68, 'Jefatura de Pediatria', 'JP', 8),
	(69, 'Coordinacion Medica', 'CM', 8),
	(70, 'Subdireccion Medica', 'SM', 8),
	(71, 'Finanzas', 'FI', 17),
	(72, 'Informatica', 'IN', 19),
	(73, 'Administracion', 'AD', 17),
	(74, 'Abasto', 'CO', 2),
	(75, 'Normativa', 'NO', 8),
	(76, 'Servicios Integrales', 'SI', 7),
	(77, 'Servicios Complementarios', 'SC', 3),
	(78, 'Transportes Y Viaticos', 'TV', 3),
	(79, 'Seguridad Y Resguardo De Bien', 'SR', 3),
	(80, 'Centro De Seguridad Social', 'CS', 14),
	(81, 'Competitividad Y Capacitacion', 'CC', 10),
	(82, 'Prestaciones A Personal', 'PP', 1),
	(83, 'Guarderia Nutricion', 'GN', 13),
	(84, 'Guarderia', 'GU', 13),
	(85, 'Laboratorio', 'LA', 7),
	(86, 'Almacen', 'AL', 2),
	(87, 'Asuntos Sindicales ', 'RL', 11),
	(88, 'Oficina De Capacitacion', 'CA', 1),
	(89, 'Prestaciones Medicas', 'PM', 8),
	(90, 'Economicas Y Sociales', 'ES', 14),
	(92, 'Transportes Y Viaticos', 'TV', 17),
	(93, 'Transportes Y Viaticos', 'TV', 12),
	(94, 'Contratos Honorarios', 'CH', 16),
	(95, 'Soporte Tecnico', 'ST', 19),
	(96, 'CoordinaciÃ³n De InformÃ¡tica', 'CI', 19),
	(97, 'Cobranza', 'CO', 12),
	(100, 'Pensiones', 'PE', 12),
	(101, 'Afiliacion Vigencia', 'AV', 12),
	(102, 'Auditoria Patrones', 'AP', 12),
	(103, 'Salud en el Trabajo', 'ST', 14),
	(104, 'Urgencias', 'UR', 8),
	(105, 'Direccion ', 'DI', 8);
/*!40000 ALTER TABLE `subareas` ENABLE KEYS */;

/*!40101 SET SQL_MODE=IFNULL(@OLD_SQL_MODE, '') */;
/*!40014 SET FOREIGN_KEY_CHECKS=IF(@OLD_FOREIGN_KEY_CHECKS IS NULL, 1, @OLD_FOREIGN_KEY_CHECKS) */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
