-- --------------------------------------------------------
-- Host:                         127.0.0.1
-- Versión del servidor:         10.1.33-MariaDB - mariadb.org binary distribution
-- SO del servidor:              Win32
-- HeidiSQL Versión:             9.4.0.5125
-- --------------------------------------------------------

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET NAMES utf8 */;
/*!50503 SET NAMES utf8mb4 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;


-- Volcando estructura de base de datos para pos
CREATE DATABASE IF NOT EXISTS `pos` /*!40100 DEFAULT CHARACTER SET utf8 COLLATE utf8_spanish_ci */;
USE `pos`;

-- Volcando estructura para tabla pos.categorias
CREATE TABLE IF NOT EXISTS `categorias` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `categoria` text COLLATE utf8_spanish_ci NOT NULL,
  `fecha` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=10 DEFAULT CHARSET=utf8 COLLATE=utf8_spanish_ci;

-- Volcando datos para la tabla pos.categorias: ~6 rows (aproximadamente)
/*!40000 ALTER TABLE `categorias` DISABLE KEYS */;
INSERT INTO `categorias` (`id`, `categoria`, `fecha`) VALUES
	(2, 'TALADROS', '2018-07-18 13:15:02'),
	(3, 'ANDAMIOS', '2018-07-18 13:18:20'),
	(5, 'GENERADORES DE ENERGÍA', '2018-07-18 13:18:49'),
	(6, 'EQUIPOS PARA CONSTRUCCIÓN', '2018-07-18 13:19:01'),
	(7, 'EQUIPOS ELECTROMECÁNICOS', '2018-07-18 21:53:20'),
	(9, 'Martillos Mecánicos', '2018-07-23 09:35:39');
/*!40000 ALTER TABLE `categorias` ENABLE KEYS */;

-- Volcando estructura para tabla pos.productos
CREATE TABLE IF NOT EXISTS `productos` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `id_categoria` int(11) NOT NULL,
  `codigo` text COLLATE utf8_spanish_ci NOT NULL,
  `descripcion` text COLLATE utf8_spanish_ci NOT NULL,
  `imagen` text COLLATE utf8_spanish_ci NOT NULL,
  `stock` int(11) NOT NULL,
  `precio_compra` float NOT NULL,
  `precio_venta` float NOT NULL,
  `ventas` int(11) NOT NULL,
  `fecha` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=64 DEFAULT CHARSET=utf8 COLLATE=utf8_spanish_ci;

-- Volcando datos para la tabla pos.productos: ~61 rows (aproximadamente)
/*!40000 ALTER TABLE `productos` DISABLE KEYS */;
INSERT INTO `productos` (`id`, `id_categoria`, `codigo`, `descripcion`, `imagen`, `stock`, `precio_compra`, `precio_venta`, `ventas`, `fecha`) VALUES
	(1, 7, '101', 'Aspiradora Industrial ', 'vistas/img/productos/101/105.png', 15, 1500, 2100, 0, '2018-07-22 23:57:01'),
	(2, 7, '102', 'Plato Flotante para Allanadora', 'vistas/img/productos/102/940.jpg', 20, 4500, 6300, 0, '2018-07-22 23:57:01'),
	(3, 7, '103', 'Compresor de Aire para pintura', 'vistas/img/productos/103/763.jpg', 20, 3000, 4200, 0, '2018-07-22 23:57:01'),
	(4, 7, '104', 'Cortadora de Adobe sin Disco ', 'vistas/img/productos/104/957.jpg', 7, 4000, 5600, 0, '2018-07-22 23:57:01'),
	(5, 7, '105', 'Cortadora de Piso sin Disco ', 'vistas/img/productos/105/630.jpg', 20, 1540, 2156, 0, '2018-07-22 23:57:01'),
	(6, 7, '106', 'Disco Punta Diamante ', '', 20, 1100, 1540, 0, '2018-07-22 11:10:13'),
	(7, 7, '107', 'Extractor de Aire ', '', 20, 1540, 2156, 0, '2018-07-22 11:10:13'),
	(8, 7, '108', 'Guada?adora ', '', 20, 1540, 2156, 0, '2018-07-22 11:10:13'),
	(9, 7, '109', 'Hidrolavadora El?ctrica ', '', 20, 2600, 3640, 0, '2018-07-22 11:10:13'),
	(10, 7, '110', 'Hidrolavadora Gasolina', '', 20, 2210, 3094, 0, '2018-07-22 11:10:13'),
	(11, 7, '111', 'Motobomba a Gasolina', '', 20, 2860, 4004, 0, '2018-07-22 11:10:13'),
	(12, 7, '112', 'Motobomba El?ctrica', '', 20, 2400, 3360, 0, '2018-07-22 11:10:13'),
	(13, 7, '113', 'Sierra Circular ', '', 20, 1100, 1540, 0, '2018-07-22 11:10:13'),
	(14, 7, '114', 'Disco de tugsteno para Sierra circular', '', 20, 4500, 6300, 0, '2018-07-22 11:10:13'),
	(15, 7, '115', 'Soldador Electrico ', '', 20, 1980, 2772, 0, '2018-07-22 11:10:13'),
	(16, 7, '116', 'Careta para Soldador', '', 20, 4200, 5880, 0, '2018-07-22 11:10:13'),
	(17, 7, '117', 'Torre de iluminacion ', '', 20, 1800, 2520, 0, '2018-07-22 11:10:13'),
	(18, 2, '201', 'Martillo Demoledor de Piso 110V', '', 20, 5600, 7840, 0, '2018-07-22 11:10:13'),
	(19, 2, '202', 'Muela o cincel martillo demoledor piso', '', 20, 9600, 13440, 0, '2018-07-22 11:10:13'),
	(20, 2, '203', 'Taladro Demoledor de muro 110V', '', 20, 3850, 5390, 0, '2018-07-22 11:10:13'),
	(21, 2, '204', 'Muela o cincel martillo demoledor muro', '', 20, 9600, 13440, 0, '2018-07-22 11:10:13'),
	(22, 2, '205', 'Taladro Percutor de 1/2 pulg. Madera y Metal', '', 20, 8000, 11200, 0, '2018-07-22 23:07:42'),
	(23, 2, '206', 'Taladro Percutor SDS Plus 110V', '', 20, 3900, 5460, 0, '2018-07-22 11:10:13'),
	(24, 2, '207', 'Taladro Percutor SDS Max 110V (Mineria)', '', 20, 4600, 6440, 0, '2018-07-22 11:10:13'),
	(25, 3, '301', 'Andamio colgante', '', 20, 1440, 2016, 0, '2018-07-22 11:10:13'),
	(26, 3, '302', 'Distanciador andamio colgante', '', 20, 1600, 2240, 0, '2018-07-22 11:10:13'),
	(27, 3, '303', 'Marco andamio modular ', '', 20, 900, 1260, 0, '2018-07-22 11:10:13'),
	(28, 3, '304', 'Marco andamio tijera', '', 20, 100, 140, 0, '2018-07-22 11:10:13'),
	(29, 3, '305', 'Tijera para andamio', '', 20, 162, 227, 0, '2018-07-22 11:10:13'),
	(30, 3, '306', 'Escalera interna para andamio', '', 20, 270, 378, 0, '2018-07-22 11:10:13'),
	(31, 3, '307', 'Pasamanos de seguridad', '', 20, 75, 105, 0, '2018-07-22 11:10:13'),
	(32, 3, '308', 'Rueda giratoria para andamio', '', 20, 168, 235, 0, '2018-07-22 11:10:13'),
	(33, 3, '309', 'Arnes de seguridad', '', 20, 1750, 2450, 0, '2018-07-22 11:10:13'),
	(34, 3, '310', 'Eslinga para arnes', '', 20, 175, 245, 0, '2018-07-22 11:10:13'),
	(35, 3, '311', 'Plataforma Met?lica', '', 20, 420, 588, 0, '2018-07-22 11:10:13'),
	(36, 5, '401', 'Planta Electrica Diesel 6 Kva', '', 20, 3500, 4900, 0, '2018-07-22 11:10:13'),
	(37, 5, '402', 'Planta Electrica Diesel 10 Kva', '', 20, 3550, 4970, 0, '2018-07-22 11:10:13'),
	(38, 5, '403', 'Planta Electrica Diesel 20 Kva', '', 20, 3600, 5040, 0, '2018-07-22 11:10:13'),
	(39, 5, '404', 'Planta Electrica Diesel 30 Kva', '', 20, 3650, 5110, 0, '2018-07-22 11:10:13'),
	(40, 5, '405', 'Planta Electrica Diesel 60 Kva', '', 20, 3700, 5180, 0, '2018-07-22 11:10:13'),
	(41, 5, '406', 'Planta Electrica Diesel 75 Kva', '', 20, 3750, 5250, 0, '2018-07-22 11:10:13'),
	(42, 5, '407', 'Planta Electrica Diesel 100 Kva', '', 20, 3800, 5320, 0, '2018-07-22 11:10:13'),
	(43, 5, '408', 'Planta Electrica Diesel 120 Kva', '', 20, 3850, 5390, 0, '2018-07-22 11:10:13'),
	(44, 6, '501', 'Escalera de Tijera Aluminio ', '', 20, 350, 490, 0, '2018-07-22 11:10:13'),
	(45, 6, '502', 'Extension Electrica ', '', 20, 370, 518, 0, '2018-07-22 11:10:13'),
	(46, 6, '503', 'Gato tensor', '', 20, 380, 532, 0, '2018-07-22 11:10:13'),
	(47, 6, '504', 'Lamina Cubre Brecha ', '', 20, 380, 532, 0, '2018-07-22 11:10:13'),
	(48, 6, '505', 'Llave de Tubo', '', 20, 480, 672, 0, '2018-07-22 11:10:13'),
	(49, 6, '506', 'Manila por Metro', '', 20, 600, 840, 0, '2018-07-22 11:10:13'),
	(50, 6, '507', 'Polea 2 canales', '', 20, 900, 1260, 0, '2018-07-22 11:10:13'),
	(51, 6, '508', 'Tensor', '', 20, 100, 140, 0, '2018-07-22 11:10:13'),
	(52, 6, '509', 'Bascula ', '', 20, 130, 182, 0, '2018-07-22 11:10:13'),
	(53, 6, '510', 'Bomba Hidrostatica', '', 20, 770, 1078, 0, '2018-07-22 11:10:13'),
	(54, 6, '511', 'Chapeta', '', 20, 660, 924, 0, '2018-07-22 11:10:13'),
	(55, 6, '512', 'Cilindro muestra de concreto', '', 20, 400, 560, 0, '2018-07-22 11:10:13'),
	(56, 6, '513', 'Cizalla de Palanca', '', 20, 450, 630, 0, '2018-07-22 11:10:13'),
	(57, 6, '514', 'Cizalla de Tijera', '', 20, 580, 812, 0, '2018-07-22 11:10:13'),
	(58, 6, '515', 'Coche llanta neumatica', '', 20, 420, 588, 0, '2018-07-22 11:10:13'),
	(59, 6, '516', 'Cono slump', '', 20, 140, 196, 0, '2018-07-22 11:10:13'),
	(60, 6, '517', 'Cortadora de Baldosin', '', 20, 930, 1302, 0, '2018-07-22 11:10:13'),
	(61, 9, '901', 'Super Martillo', 'vistas/img/productos/default/anonymous.png', 20, 800, 1120, 0, '2018-07-23 15:07:36'),
	(62, 2, '208', 'Taladro Primario', 'vistas/img/productos/default/anonymous.png', 52, 486.52, 681.128, 0, '2018-07-24 08:37:00'),
	(63, 9, '902', 'Martillo de Thor', 'vistas/img/productos/902/633.jpg', 15, 789.26, 1104.96, 0, '2018-07-24 10:08:45');
/*!40000 ALTER TABLE `productos` ENABLE KEYS */;

-- Volcando estructura para tabla pos.usuarios
CREATE TABLE IF NOT EXISTS `usuarios` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `nombre` text COLLATE utf8_spanish_ci NOT NULL,
  `usuario` text COLLATE utf8_spanish_ci NOT NULL,
  `password` text COLLATE utf8_spanish_ci NOT NULL,
  `perfil` text COLLATE utf8_spanish_ci NOT NULL,
  `foto` text COLLATE utf8_spanish_ci NOT NULL,
  `estado` int(11) NOT NULL,
  `ultimo_login` datetime NOT NULL,
  `fecha` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=12 DEFAULT CHARSET=utf8 COLLATE=utf8_spanish_ci;

-- Volcando datos para la tabla pos.usuarios: ~7 rows (aproximadamente)
/*!40000 ALTER TABLE `usuarios` DISABLE KEYS */;
INSERT INTO `usuarios` (`id`, `nombre`, `usuario`, `password`, `perfil`, `foto`, `estado`, `ultimo_login`, `fecha`) VALUES
	(1, 'Administrador', 'admin', '$2a$07$asxx54ahjppf45sd87a5auXBm1Vr2M1NV5t/zNQtGHGpS5fFirrbG', 'Administrador', 'vistas/img/usuarios/admin/737.jpg', 1, '2018-07-29 20:42:03', '2018-07-29 20:42:03'),
	(5, 'Ana Gonzalez', 'ana', '$2a$07$asxx54ahjppf45sd87a5auzGfz9GaOjSPJ5jEDpHii9vSQEEqY1Zm', 'Vendedor', 'vistas/img/usuarios/ana/991.png', 1, '2018-07-17 12:27:22', '2018-07-17 12:27:22'),
	(7, 'beca santoyo', 'beca', '$2a$07$asxx54ahjppf45sd87a5au3D8ndMFoKyiFuOX.oCJzaqiHwtOZ3IW', 'Especial', 'vistas/img/usuarios/beca/919.jpg', 1, '0000-00-00 00:00:00', '2018-07-13 21:00:38'),
	(8, 'Juan fernando Urrego', 'juan', '$2a$07$asxx54ahjppf45sd87a5au.U/M0caGNRi1j0bgxZqVwBDctNLt11O', 'Especial', 'vistas/img/usuarios/juan/449.jpg', 1, '0000-00-00 00:00:00', '2018-07-18 08:54:51'),
	(9, 'Julio fernandez', 'julio', '$2a$07$asxx54ahjppf45sd87a5aub5nGP0VwzLwk9mYKNeU59ocPXY/HdwK', 'Administrador', 'vistas/img/usuarios/julio/753.jpg', 1, '0000-00-00 00:00:00', '2018-07-18 09:09:27'),
	(10, 'soledad ramirez', 'sol', '$2a$07$asxx54ahjppf45sd87a5auN6jspVRrjZPLwDpMHMru1/UUC64bloW', 'Especial', 'vistas/img/usuarios/sol/823.jpg', 1, '0000-00-00 00:00:00', '2018-07-18 09:08:30'),
	(11, 'Bibiana González Cota', 'bif', '$2a$07$asxx54ahjppf45sd87a5aum69.kNY00aIcBFCXN9JwrvMS/w/Arxq', 'Administrador', 'vistas/img/usuarios/bif/221.jpg', 1, '2018-07-20 15:03:14', '2018-07-20 15:03:14');
/*!40000 ALTER TABLE `usuarios` ENABLE KEYS */;

/*!40101 SET SQL_MODE=IFNULL(@OLD_SQL_MODE, '') */;
/*!40014 SET FOREIGN_KEY_CHECKS=IF(@OLD_FOREIGN_KEY_CHECKS IS NULL, 1, @OLD_FOREIGN_KEY_CHECKS) */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
